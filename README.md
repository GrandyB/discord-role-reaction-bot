# Discord Role Reaction Bot

Hello there - if you are here, you have likely seen or heard about "reaction" bots, which are making the process for members to join certain roles on certain servers a much easier process.
The idea is that you setup a bunch of messages, perhaps with an 'overview' image, and you have a bot that listens for certain emotes to be posted as 'reactions' on that message. When they do, that emote is looked up and a role is applied to the user in question.
It's a very neat little system but as of yet I haven't seen any opensourced.

Recently, while creating a Discord bot for a Rocket League community I help run (Division6 - http://discord.division6.org), I decided I would make it in such a way that it should be easy to extract out this functionality and eventually opensource that bit.
So here it is, I hope it is useful - some of it is rather skeleton, but should do the job. 

Find in the sub-folder(s) the various implementation(s) of the bot.