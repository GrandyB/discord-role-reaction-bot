
//	Copyright (C) 2017 Mark "Grandy" Bishop

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * bot.js
 * Skeleton main file, which delegates message and reaction events to RoleReaction.js,
 * where it is then acted upon. 
 */
var Discord = require('discord.io');
const RoleReaction = require('./RoleReaction.js');
var settings = require('./settings.json');

var bot = new Discord.Client({
	token: settings.token,
	autorun: true
});

var roleReaction = new RoleReaction(bot, settings.serverID);

bot.on("ready", function(event) {
	console.log("Connected!");
	console.log("Logged in as: ");
	console.log(bot.username + " - (" + bot.id + ")");

	roleReaction.init();
});


bot.on("message", function(user, userID, channelID, message, event) {
	roleReaction.main(user, userID, channelID, message, event);
});

bot.on("any", function(event) {
	//console.log(event); //Logs every event
	if(event.t == "MESSAGE_REACTION_ADD") {
		roleReaction.processReactionAdd(event.d);
	} else if(event.t == "MESSAGE_REACTION_REMOVE") {
		roleReaction.processReactionRemove(event.d);
	}
});

bot.on("disconnect", function() {
	console.log("Bot disconnected");
	bot.connect(); // Attempt to auto reconnect
});