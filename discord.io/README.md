# Discord.io implementation

This was made through nodejs / https://github.com/izy521/discord.io
Shouldn't need to pre-install anything - the package.json through node should install everything needed to run it.

## Basic workflow
1. Create a Discord bot application, authorize it on your server
2. Install nodejs on the system, run command 'npm install' within application directory
3. Post several messages (or images) in a channel
4. Add some custom emotes to your server to represent the role choices
5. Fill out the settings.json file with the relevant details
6. Run the bot

So, to go over that step-by-step:

### Configuring the bot settings
This will cover steps 3, 4 and 5.
Once you've downloaded the repository, take a look at settings.json and by default you'll find something like this:
```
{
	"token" : "",
	"serverID" : "98989898989898989898",
	"channelID" : "75757575757575757575",
	"adminRoleIDs" : ["10101010110010101"],
	"messages" : [
		{
			"messageID" : "987987987987987987987",
			"options" : [{
				"emote" : "<:custom_emoji1:321321321321321321>",
				"roleID" : "123456789123456789"
			}, {
				"emote" : "<:custom_emoji2:123123123123123123>",
				"roleID" : "987654321987654321"
			}]
		}, {
			"messageID" : "567567567567567567567",
			"options" : [{
				"emote" : "<:custom_emoji3:432432432432432432>",
				"roleID" : "678678678678678678"
			}]
		}
	]
}
```
- "token" - *from your Discord bot application page on the Discord site*
- "serverID" - *right click on your server -> copy ID (might need Discord Canary or enable dev mode? not sure)*
- "channelID" - *right click on the channel you want this functionality in -> copy ID*
- "adminRoleIDs" - *this is a bit harder. First, make whichever admin role **mentionable**. Then do `\@RoleName` and it will spew out something like `<@&123456789123456789>`. Just take the numbered bit and place in there*

Then there's the messages section. This bot does not post the source messages - that's up to you. 

- "messageID" - *post a message or an overview image, then click the little three vertical ... menu on that message, copy ID.*
- "emote" - *assuming you've added custom emotes to represent your roles, do `\:emote_name:` like you did for adminRoleIDs, and put the result into the settings file.*
- "roleID" - *use the same method for getting the adminRoleID to get the id of the role you wish the emote to map to, remembering that it requires that role to be mentionable **(which you can turn off afterwards, no problem)***

### Creating the bot user and running it
This will cover steps 1, 2 and 6.

#### Discord Application
You'll need to give the bot a user.
- Go [here](https://discordapp.com/developers/applications/me), give your bot a name and save
- On the next page, click 'create a bot user'
- Authorize your bot (add your bot to your server)
 - Open this in your browser (but don't navigate there yet)` https://discordapp.com/oauth2/authorize?&client_id=<CLIENT_ID_HERE>&scope=bot&permissions=268435456` (bot needs role management permissions)
 - Go back to the application page and copy your 'client ID', replacing it where `<CLIENT_ID_HERE>` is in your other tab
 - Navigate to that page. Should give you an "Authorize access to your account, give you a list of servers you could add it to, and has the 'Manage Roles' option pre-selected
 - **NOTE:** You'll want to keep this page open, as we'll need the *token* later

#### Installing the bot on the system
Javascript (and specifically nodejs) runs on just about any system, and is super simple to setup on anything, which I think is why I initially started writing small bots using it!
I host my bots on DigitalOcean, using a node module called 'forever' to keep/restart the application.

nodejs download: https://nodejs.org/en/download/

Once you have nodejs on your system, navigate to where you've downloaded this repository to on the command line, then do 'npm install'.
This will fetch the dependencies listed in 'package.json' (just discord.io) and install those.

#### Running the bot
To run the bot, simply be in a command line in the application folder and type:
`node Bot.js`

## ...and that should be it!
After startup, should all the permissions and settings be correct, you should see your emotes magically being added to the messages you defined, and upon selecting it should add you to the relevant roles.
At any time, you should be able to change the settings file and do !init in the channel to refresh the mappings.. might need some cleanup, and feel free to restart the bot if that doesn't work.

Any other questions, please raise an issue on this repository and I'll try to answer it whenever I can. 
Alternatively, my Discord username is `Grandy #0243` if you'd like to have a chat.

Cheers and good luck!