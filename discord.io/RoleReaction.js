
//	Copyright (C) 2017 Mark "Grandy" Bishop

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * RoleReaction.js
 * The main brains behind tying the reactions to the roles.
 */

var bot, serverID;
var reactSettings = require('./settings.json');
var interval = 1300; 			// milliseconds between adding new reactions, 1300 seems to work reliably
var isSending = false;			// whether we are currently sending reactions
var sendReactionQueue = [];		// our queue of reactions
var emoteToRoleMap = [];		// an array of { emote: 'etc', roleID: 'etc' }
var messageIDs = [];			// message IDs in our "watchlist", those we are looking for reactions on

function RoleReaction(botRef, serverIDRef) {
	bot = botRef;
	serverID = serverIDRef;
}

/** Main message event loop, called in the 'bot.on("message")' function in the main file. */
RoleReaction.prototype.main = function(user, userID, channelID, message, event) {
	var messageID = event.d.id;

	// Handle command(s)
	if(isAuthorized(userID)) {
		if(message.startsWith("!init")) {
			this.init();
			wipeCall(messageID, channelID);
		}
	}

	return;
}

RoleReaction.prototype.init = function() {
	// Reload the settings file
	reactSettings = require('./settings.json');
	var messages = reactSettings.messages;

	// Ensure that each entry has a message ID
	for(i = 0; i < messages.length; i++) {
		var message = messages[i];
		if(message.messageID == "") {
			console.log("Nothing to reload, messageIDs not set in config file.");
			return;
		}
	}

	console.log("----------\nLoading config...\n----------");
	for(i = 0; i < messages.length; i++) {
		var message = messages[i];
		messageIDs.push(message.messageID);
		console.log("MESSAGE ID: '"+message.messageID+"'");

		for(j = 0; j < message.options.length; j++) {
			var option = message.options[j];
			console.log("MAPPING | emote:'"+option.emote+"' -> roleID: '"+option.roleID+"'");
			addRoleMapping(option.emote, option.roleID);
			var convertedEmote = convertTaggedEmoteToReactionEmote(option.emote);

			sendReactionQueue.push({
				"channelID" : reactSettings.channelID, 
				"messageID" : message.messageID, 
				"reaction" : convertedEmote
			});

			if(!isSending) {
				reactionTick(); 
			}
			
		}
	}
}

/**
 * Process a raw MESSAGE_REACTION_ADD event.
 * @param d - { 
 *		user_id, 
 *		message_id, 
 *		emoji : {
 *			name, id
 *		},
 *		channel_id
 *	}
 */
RoleReaction.prototype.processReactionAdd = function(d) {
	if(includes(messageIDs, d.message_id)) {
		addUserToRoleID(d.user_id, getRoleIDForEmote(d.emoji.id));
	}
}

/**
 * Process a raw MESSAGE_REACTION_REMOVE event.
 * @param d - { 
 *		user_id, 
 *		message_id, 
 *		emoji : {
 *			name, id
 *		},
 *		channel_id
 *	}
 */
RoleReaction.prototype.processReactionRemove = function(d) {
	if(includes(messageIDs, d.message_id)) {
		removeUserFromRoleID(d.user_id, getRoleIDForEmote(d.emoji.id));
	}
}

module.exports = RoleReaction;

////////////// HELPER/NON-EXPORTED METHODS //////////////

/**
 * Repeatable function, called by itself until the reaction queue is empty.
 * Used to initialise/reload the reactions onto the messages, based on the config in settings.json.
 */
var reactionTick = function() {
	if(sendReactionQueue.length == 0) {
		isSending = false;
		console.log("----------\nFinished adding reactions.\n----------");
		return;
	}

	isSending = true;
	var input = sendReactionQueue.shift();
	console.log("ADDING REACTION | "+JSON.stringify(input));
	bot.addReaction(input);

	setTimeout(reactionTick, interval);

}

/** 
 * Add an emote<->role mapping.
 *
 * @param emote 	e.g. "<:custom_emoji1:321321321321321321>"
 * @param roleID 	e.g. "123456789123456789"
 * (same as that in settings.json)
 */
function addRoleMapping(emote, roleID) {
	var item = { "emote" : emote, "roleID" : roleID };
	emoteToRoleMap.push(item);
}

/**
 * Using the emote<->roleID map, find the role ID for a given emoteID.
 * 
 * @param emoteID 	The emote ID (can be partial or full)
 */
function getRoleIDForEmote(emoteID) {
	var map = "";
	for(var i=0; i<emoteToRoleMap.length; i++) {
		var item = emoteToRoleMap[i];

		// Build up a debug map incase of errors
		map += item.emote+" -> "+item.roleID+",\n";

		if(includes(item.emote, emoteID)) {
			return item.roleID;
		}
	}
	console.error("Unable to find RoleID for emote: "+emoteID+".\nHere is the current map:");
	console.error(map);
}

function addUserToRoleID(userID, roleID) {
	if(userID == bot.id) {
		return;
	}

	var packet = {
		"serverID" : serverID,
		"userID" : userID,
		"roleID": roleID
	};

	bot.addToRole(packet, function(err,response) {
    	if (err) {
    		console.log(JSON.stringify(packet));
    		console.error(err); /* Failed to apply role */
    	}
    });
}

function removeUserFromRoleID(userID, roleID) {
	if(userID == bot.id) {
		return;
	}

	var packet = {
		"serverID" : serverID,
		"userID" : userID,
		"roleID": roleID
	};

	bot.removeFromRole(packet, function(err,response) {
    	if (err) {
    		console.log(packet);
    		console.error(err); /* Failed to apply role */
    	}
    });
}

/** 
 * Use admin role IDs from settings.json to check if the given user is an authorized user.
 */
function isAuthorized(userID) {
	var member = bot.servers[serverID].members[userID];
	if(member == null) {
		return false;
	}
	return arrContains(reactSettings.adminRoleIDs, member.roles);
}

/** Remove a message from a channel. */
function wipeCall(messageID, channelID) {
	bot.deleteMessage({
		channelID: channelID,
		messageID: messageID
	});
}

/**
 * Convert a tagged (e.g. "<:notif_general:344236451409428480>") emote to the needed
 * reaction emote (e.g. "notif_general:344236451409428480").
 */
function convertTaggedEmoteToReactionEmote(emote) {
	emote = emote.substr(2);
	emote = emote.substr(0, emote.length-1);
	return emote;
}

/** A workaround for earlier versions of javascript - later versions should be able to use arr.includes(val); */
function arrContains(haystack, needles) {
    return needles.some(function (needle) {
        return haystack.indexOf(needle) >= 0;
    });
};

function includes(arr, val) {
	return arr.indexOf(val) != -1;
}